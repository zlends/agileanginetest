import { StyleSheet } from "react-native";

const styles: any = StyleSheet.create({
  page: {
    flex: 1,
    alignItems: "stretch",
    backgroundColor: "black"
  },
  errorContainer: {
    padding: 30,
    alignItems: "center"
  },
  errorText: {
    textAlign: "center"
  },
  item: {
    paddingLeft: 5,
    paddingRight: 5,
    paddingTop: 5,
    paddingBottom: 5
  },
  indicator: {
    position: "absolute",
    alignSelf: "center",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: "center",
    alignItems: "center"
  }
});
export default styles;
