import {
  getPictures,
  obtainToken
} from "../../services/API";
import type {
  ActionWithPayload,
  ActionWithoutPayload
} from "../../types/actions";
import { saveToken } from "../../services/LocalSotrage";
import type { ImagesResponse } from "../../services/Types";

export const PICTURES_FETCH_REQUESTED = "PICTURES_FETCH_REQUESTED";
export const PICTURES_FETCH_SUCCESS = "PICTURES_FETCH_SUCCESS";
export const FETCH_FAILED = "FETCH_FAILED";

export function listIsLoading(): ActionWithoutPayload {
  return {
    type: PICTURES_FETCH_REQUESTED,
    isLoading: true
  };
}

export function fetchListSuccess(
  pictures: Array<Object>,
  page: number
): ActionWithPayload {
  return {
    type: PICTURES_FETCH_SUCCESS,
    payload: {
      pictures,
      page
    }
  };
}

export function fetchListFailed(error: string): ActionWithPayload {
  return {
    type: FETCH_FAILED,
    payload: {
      error
    }
  };
}

export function fetchPictures(page: number = 1) {
  return async dispatch => {
    dispatch(listIsLoading());
    try {
      const authResponse = await obtainToken(page);
      await saveToken(authResponse.token);

      const imagesResponse: ImagesResponse = await getPictures(page);
      dispatch(fetchListSuccess(imagesResponse.pictures, page));
    } catch ({ errorMessage }) {
      dispatch(fetchListFailed(errorMessage));
    }
  };
}
