// @flow
import * as React from "react";
import {Platform, StatusBar} from "react-native";
import {connect} from "react-redux";

import HomeView from "../../screens/Home";
import {fetchPictures} from "./actions";

export interface Props {
    navigation: any;
    fetchPictures: Function;
    pictures: Array<Object>;
    isLoading: boolean;
}

export interface State {
}

class HomeContainer extends React.Component<Props, State> {
    static navigationOptions = {
        header: null
    };

    constructor(props) {
        super(props);
        StatusBar.setBarStyle("light-content");
        Platform.OS === "android" && StatusBar.setBackgroundColor("#000");
    }

    componentWillMount() {
        this.onRefresh();
    }

    onRefresh = () => {
        this.props.fetchPictures(1);
    };

    onLoadNext = ({distanceFromEnd}) => {
      console.log('distanceFromEnd', distanceFromEnd)
        const {isLoading, fetchPictures, page} = this.props;
        if (!isLoading) {
            fetchPictures(page + 1);
        }
    };

    render() {
        return (
            <HomeView
                {...this.props}
                onRefresh={this.onRefresh}
                onLoadNext={this.onLoadNext}
            />
        );
    }
}

const mapStateToProps = ({homeReducer}) => ({
    pictures: homeReducer.pictures,
    page: homeReducer.page,
    isLoading: homeReducer.isLoading
});

export default connect(mapStateToProps, {fetchPictures})(HomeContainer);
