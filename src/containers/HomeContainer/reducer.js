// @flow
import {
  FETCH_FAILED,
  PICTURES_FETCH_REQUESTED,
  PICTURES_FETCH_SUCCESS
} from "./actions";

const initialState = {
  pictures: [],
  isLoading: false,
  page: 1,
  errorMessage: ""
};

export default function(state: any = initialState, action: Object) {
  const { payload } = action;
  switch (action.type) {
    case PICTURES_FETCH_SUCCESS:
      return {
        ...state,
        pictures:
          payload.page === 1
            ? payload.pictures
            : [...state.pictures, ...payload.pictures],
        isLoading: false,
        page: payload.page,
        errorMessage: ""
      };
    case FETCH_FAILED:
      return {
        ...state,
        pictures: [],
        isLoading: false,
        page: 1,
        errorMessage: payload.errorMessage
      };
    case PICTURES_FETCH_REQUESTED:
      return {
        ...state,
        isLoading: true
      };
    default:
      return state;
  }
}
