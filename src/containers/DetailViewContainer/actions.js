// @flow
import {FETCH_FAILED} from "../HomeContainer/actions";
import type {
    ActionWithPayload,
    ActionWithoutPayload
} from "../../types/actions";
import {getPictureDetails} from "../../services/API";
import type {DetailedPicture} from "../../services/Types";

export const PICTURE_DETAILS_FETCH_REQUESTED =
    "PICTURE_DETAILS_FETCH_REQUESTED";
export const PICTURE_DETAILS_FETCH_SUCCESS = "PICTURE_DETAILS_FETCH_SUCCESS";

export function pictureIsLoading(): ActionWithoutPayload {
    return {
        type: PICTURE_DETAILS_FETCH_REQUESTED
    };
}

export function fetchPictureSuccess(
    picture: DetailedPicture
): ActionWithPayload {
    return {
        type: PICTURE_DETAILS_FETCH_SUCCESS,
        payload: {picture}
    };
}

export function fetchPictureFailed(error: string): ActionWithPayload {
    return {
        type: FETCH_FAILED,
        payload: {
            error
        }
    };
}

export function fetchPictureDetails(imageId: number) {
    return async dispatch => {
        dispatch(pictureIsLoading());
        try {
            const detailedPicture: DetailedPicture = await getPictureDetails(imageId);
            dispatch(fetchPictureSuccess(detailedPicture));
        } catch ({errorMessage}) {
            dispatch(fetchPictureFailed(errorMessage));
        }
    };
}
