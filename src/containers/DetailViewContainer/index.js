// @flow
import * as React from "react";
import DetailView from "../../screens/DetailView";
import {connect} from "react-redux";
import {fetchPictureDetails} from "./actions";
import type {DetailedPicture} from "../../services/Types";
import {ActivityIndicator} from "react-native";
import styles from "../../screens/Home/styles";

export interface Props {
    navigation: any;
    fetchPictureDetails: Function;
    isLoading: boolean;
    picture: DetailedPicture;
}

export interface State {
    imageUrl: string;
}

class DetailViewContainer extends React.Component<Props, State> {
    static navigationOptions = {
        headerStyle: {
            backgroundColor: "black",
            position: "absolute",
            height: 50,
            top: 0,
            left: 0,
            right: 0,
            borderBottomWidth: 0
        },
        headerTintColor: "black"
    };

    componentDidMount() {
        const {navigation, fetchPictureDetails} = this.props;
        const {pictureDetails: picture} = navigation.state.params;

        fetchPictureDetails(picture.id);
    }

    share = (imageId: number): void => {
        // TODO: implement share function
    };

    applyFilter = (type): void => {
        // TODO: implement apply image filter function
    };

    render() {
        const {isLoading, picture} = this.props;
        if (picture === null) return <ActivityIndicator style={styles.indicator} />;
        return (
            <DetailView
                key={picture.id}
                imageUrl={picture.full_picture || ""}
                pictureDetails={{id: picture.id}}
                shareCallback={this.share}
                isLoading={isLoading}
                applyFilterCallback={this.applyFilter}
            />
        );
    }
}

const mapStateToProps = ({detailViewReducer}) => ({
    picture: detailViewReducer.picture,
    isLoading: detailViewReducer.isLoading,
    error: detailViewReducer.error
});

export default connect(mapStateToProps, {fetchPictureDetails})(
    DetailViewContainer
);
