import {
  PICTURE_DETAILS_FETCH_REQUESTED,
  PICTURE_DETAILS_FETCH_SUCCESS
} from "./actions";
import { FETCH_FAILED } from "../HomeContainer/actions";
import type {DetailedPicture} from "../../services/Types";

export type DetailViewStore = {
  picture: DetailedPicture,
  isLoading: boolean,
  error: string
}

const initialState: DetailViewStore = {
  picture: null,
  isLoading:  false,
  error: ""
};

export default function(state: any = initialState, action: Object) {
  const { payload } = action;
  switch (action.type) {
    case PICTURE_DETAILS_FETCH_SUCCESS:
      return {
        ...state,
        picture: payload.picture,
        isLoading: false,
        error: ""
      };
    case FETCH_FAILED:
      return {
        ...state,
        isLoading: false,
        errorMessage: payload.errorMessage
      };
    case PICTURE_DETAILS_FETCH_REQUESTED:
      return {
        ...state,
        isLoading: true
      };
    default:
      return state;
  }
}
