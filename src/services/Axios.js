// @flow
import axios, {AxiosInstance, AxiosRequestConfig} from 'axios'

const API_ENDPOINT = 'http://195.39.233.28:8035'

const instance: AxiosInstance = axios.create({
    baseURL: API_ENDPOINT,
    headers: {
        'Content-Type': 'application/json',
    },
    timeout: 16000,
});


instance.interceptors.response.use(
    response => response,
    async error => {
        console.log(error)
        return await Promise.reject(error)
    },
)

const get = <R>(url: string, config?: AxiosRequestConfig): Promise<R> =>
    instance.get(url, config).then(({data}) => data)
const post = <D, R>(url: string, data?: D, config?: AxiosRequestConfig): Promise<R> =>
    instance.post(url, data, config).then(({data}) => data)
const put = <D, R>(url: string, data?: D, config?: AxiosRequestConfig): Promise<R> =>
    instance.put(url, data, config).then(({data}) => data)
const patch = <D, R>(url: string, data?: D, config?: AxiosRequestConfig): Promise<R> =>
    instance.patch(url, data, config).then(({data}) => data)
const _delete = <R>(url: string, config?: AxiosRequestConfig): Promise<R> =>
    instance.delete(url, config).then(({data}) => data)

export {get, post, put, patch, _delete}
