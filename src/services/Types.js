export type AuthResponse =
    { token: string }

export type ImagesResponse =
    {
        hasMore: boolean,
        page: number,
        pageCount: number,
        pictures: CroppedImage[],
    }

export type CroppedImage =
    {
        id: string,
        cropped_picture: string
    }

export type DetailedPicture =
    {
        id: string,
        author: string
        camera: string
        cropped_picture: string
        full_picture: string
    }
