import {AsyncStorage} from 'react-native';

const STORAGE_TOKEN_KEY = 'STORAGE_TOKEN_KEY'

export async function saveToken(token: string) {
     return AsyncStorage.setItem(STORAGE_TOKEN_KEY, token)
}

export async function getToken(): string {
    return  AsyncStorage.getItem(STORAGE_TOKEN_KEY)
}
