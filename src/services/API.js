import type {AuthResponse, DetailedPicture, ImagesResponse} from "./Types";
import {get, post} from "./Axios";
import {getToken} from "./LocalSotrage";

const API_KEY = '23567b218376f79d9415'
const AUTH_PATH = 'auth';
const IMAGES_PATH = 'images';


export async function obtainToken(): AuthResponse {
    return post(AUTH_PATH, {"apiKey": API_KEY})
}

export async function getPictures(page: number = 1): Promise<ImagesResponse> {
    const token = await getToken()
    return get(`${IMAGES_PATH}?page=${page}`, {headers: {Authorization: `Bearer ${token}`}})
}

export async function getPictureDetails(id: number): Promise<DetailedPicture> {
    const token = await getToken()
    return get(`${IMAGES_PATH}/${id}`, {headers: {Authorization: `Bearer ${token}`}})
}
